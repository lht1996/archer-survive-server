package com.lht.archersurvive.base;

import io.netty.channel.Channel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 乐浩天
 * @date 2022/8/21 5:28
 */
@Service
public class NetService {

    @Autowired
    private NettyConfig nettyConfig;

    private NettyTcpConfig config = new NettyTcpConfig();

    private Map<Long, Channel> channelMap = new HashMap<>();

    @EventListener
    public void init(ApplicationStartedEvent event) {
//        LoopResources loop = LoopResources.create("event-loop", 1, 4, true);
//        DisposableServer server = TcpServer.create().port(nettyConfig.getPort())
//                // 开启时系统会在连接空闲一定时间后像客户端发送请求确认连接是否有效
//                .childOption(ChannelOption.SO_KEEPALIVE, config.isKeepAlive())
//                // 关闭Nagle算法
//                .childOption(ChannelOption.TCP_NODELAY, config.isTcpNoDalay())
//                // 连接关闭时,尝试把示发送完成的数据继续发送,(等待几秒)
//                .childOption(ChannelOption.SO_LINGER, config.getSoLinger())
//                // 使用byteBuf池,默认不使用
//                .childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
//                // 使用byteBuf池,默认不使用
//                .childOption(ChannelOption.RCVBUF_ALLOCATOR, AdaptiveRecvByteBufAllocator.DEFAULT)
//                // 端口重用,如果开启则在上一个进程未关闭情况下也能正常启动
//                .option(ChannelOption.SO_REUSEADDR, config.isReuseAddress())
//                // 最大等待连接的connection数量
//                .option(ChannelOption.SO_BACKLOG, config.getBacklog())
//                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10000)
//                .handle(new Handler())
//                .runOn(loop)
//                .bindNow();
//        server.onDispose().block();
    }
}
