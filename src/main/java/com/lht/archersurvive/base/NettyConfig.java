package com.lht.archersurvive.base;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author 乐浩天
 * @date 2022/8/21 6:24
 */

@Configuration
@ConfigurationProperties("netty")
@Data
public class NettyConfig {
    private int port;
}
