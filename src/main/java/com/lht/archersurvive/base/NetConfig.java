package com.lht.archersurvive.base;

/**
 * 网络配置
 *
 * @author zhenkun.wei
 */
public class NetConfig {

    /**
     * 读空闲
     */
    public static int DEFAULT_READ_IDLE_TIME = 35 * 5;

    /**
     * 客户端TCP请求最大包
     */
    public static int DEFAULT_TCP_MAX_RECEIVE_LENGTH = 1024 * 1024;

    /**
     * 默认登陆消息号
     */
    public static int DEFAULT_LOGIN_MSG_CODE = 100;

    /**
     * 默认下线消息号
     */
    public static int DEFAULT_DISCONNECT_MSG_CODE = 200;

    /**
     * 默认重连消息号
     */
    public static int DEFAULT_RECONNECT_MSG_CODE = 300;

    /**
     * 最大缓存客户端请求数量
     */
    public static int DEFAULT_MAX_RESPONSE_CACHE_COUNT = 50;

    /**
     * 最大缓存客户端PUSH数量
     */
    public static int DEFAULT_MAX_PUSH_CACHE_COUNT = 30;

    /**
     * 网络层明文调试
     */
    public static boolean DEFAULT_NET_MSG_CLEAR_DEBUG = false;

    /**
     * 网络层非压缩调试
     */
    public static boolean DEFAULT_NET_MSG_UNCOMPRESS_DEBUG = false;

    /**
     * http客户端允许最大包默认值
     */
    public static int DEFAULT_HTTP_MAX_CONTENT_LENGTH = 1024 * 100;

    /**
     * 是否使用https
     */
    public static boolean DEFAULT_USE_HTTPS = false;

    /**
     * https证书
     */
    public static String DEFAULT_HTTPS_CERT_PASSWORD = "test@123";

    /**
     * https证书路径
     */
    public static String DEFAULT_HTTPS_CERT_PATH = "";

    /**
     * 是否尝试重发push
     */
    public static boolean PUSH_RETRY = true;

    /**
     * vertx 是否使用https
     */
    public static boolean DEFAULT_VERTX_USE_HTTPS = false;

    /**
     * vertx 证书
     */
    public static String DEFAULT_VERTX_HTTPS_CERT_PASSWORD = "test@123";

    /**
     * vertx https证书路径
     */
    public static String DEFAULT_VERTX_HTTPS_CERT_PATH = "";
    /**
     * 默认TCP是否使用EPOLL
     */
    public static boolean DEFAULT_TCP_USE_EPOLL = true;
    /**
     * 默认TCP客户端是否使用EPOLL
     */
    public static boolean DEFAULT_CLIENT_TCP_USE_EPOLL = true;
    /**
     * 默认UDP是否使用EPOLL
     */
    public static boolean DEFAULT_UDP_USE_EPOLL = true;
    /**
     * 默认UDP客户端是否使用EPOLL
     */
    public static boolean DEFAULT_CLIENT_UDP_USE_EPOLL = true;
    /**
     * 默认NettyTCP SO_SNDBUF
     */
    public static int NETTY_TCP_SO_SNDBUF = 0;
    /**
     * 默认NettyTCP SO_RCVBUF
     */
    public static int NETTY_TCP_SO_RCVBUF = 0;

}
