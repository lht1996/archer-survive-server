package com.lht.archersurvive.base;

/**
 * Netty TCP配置
 *
 * @author 乐浩天
 */
public class NettyTcpConfig {

    private int bossThreadNum = 1;

    private int workerThreadNum = Runtime.getRuntime().availableProcessors() * 2;

    private boolean keepAlive = true;

    private boolean tcpNoDalay = true;

    private int soLinger = 0;

    private boolean reuseAddress = true;

    private int backlog = 10000;

    private int sendBufferSize = NetConfig.NETTY_TCP_SO_SNDBUF;

    private int receiveBufferSize = NetConfig.NETTY_TCP_SO_RCVBUF;

    private int ioRatio = 100;

    private boolean useEpoll = NetConfig.DEFAULT_TCP_USE_EPOLL;

    private boolean clientUseEpoll = NetConfig.DEFAULT_CLIENT_UDP_USE_EPOLL;

    public static NettyTcpConfig newConfig() {
        return new NettyTcpConfig();
    }

    public NettyTcpConfig bossThreadNum(int bossThreadNum) {
        this.bossThreadNum = bossThreadNum;
        return this;
    }

    public NettyTcpConfig workerThreadNum(int workerThreadNum) {
        this.workerThreadNum = workerThreadNum;
        return this;
    }

    public NettyTcpConfig keepAlive(boolean keepAlive) {
        this.keepAlive = keepAlive;
        return this;
    }

    public NettyTcpConfig tcpNoDalay(boolean tcpNoDalay) {
        this.tcpNoDalay = tcpNoDalay;
        return this;
    }

    public NettyTcpConfig soLinger(int soLinger) {
        this.soLinger = soLinger;
        return this;
    }

    public NettyTcpConfig reuseAddress(boolean reuseAddress) {
        this.reuseAddress = reuseAddress;
        return this;
    }

    public NettyTcpConfig backlog(int backlog) {
        this.backlog = backlog;
        return this;
    }

    public NettyTcpConfig sendBufferSize(int sendBufferSize) {
        this.sendBufferSize = sendBufferSize;
        return this;
    }

    public NettyTcpConfig receiveBufferSize(int receiveBufferSize) {
        this.receiveBufferSize = receiveBufferSize;
        return this;
    }

    public NettyTcpConfig ioRatio(int ioRatio) {
        this.ioRatio = ioRatio;
        return this;
    }

    public NettyTcpConfig useEpoll(boolean useEpoll) {
        this.useEpoll = useEpoll;
        return this;
    }

    public NettyTcpConfig clientUseEpoll(boolean clientUseEpoll) {
        this.clientUseEpoll = clientUseEpoll;
        return this;
    }

    public int getBossThreadNum() {
        return bossThreadNum;
    }

    public int getWorkerThreadNum() {
        return workerThreadNum;
    }

    public boolean isKeepAlive() {
        return keepAlive;
    }

    public boolean isTcpNoDalay() {
        return tcpNoDalay;
    }

    public int getSoLinger() {
        return soLinger;
    }

    public boolean isReuseAddress() {
        return reuseAddress;
    }

    public int getBacklog() {
        return backlog;
    }

    public int getSendBufferSize() {
        return sendBufferSize;
    }

    public int getReceiveBufferSize() {
        return receiveBufferSize;
    }

    public int getIoRatio() {
        return ioRatio;
    }

    public boolean isUseEpoll() {
        return useEpoll;
    }

    public void setUseEpoll(boolean useEpoll) {
        this.useEpoll = useEpoll;
    }

    public boolean isClientUseEpoll() {
        return clientUseEpoll;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("NettyTcpConfig [bossThreadNum=").append(bossThreadNum)
                .append(", workerThreadNum=").append(workerThreadNum).append(", keepAlive=")
                .append(keepAlive).append(", tcpNoDalay=").append(tcpNoDalay).append(", soLinger=")
                .append(soLinger).append(", reuseAddress=").append(reuseAddress)
                .append(", backlog=").append(backlog).append(", sendBufferSize=")
                .append(sendBufferSize).append(", receiveBufferSize=").append(receiveBufferSize)
                .append(", ioRatio=").append(ioRatio).append(", useEpoll=").append(useEpoll)
                .append(", clientUseEpoll=").append(clientUseEpoll).append("]");
        return builder.toString();
    }

}

