package com.lht.archersurvive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArcherSurviveServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArcherSurviveServerApplication.class, args);
    }

}
