package com.lht.archersurvive.biz.repository;

import com.lht.archersurvive.biz.entity.Account;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

/**
 * 账号
 *
 * @author 乐浩天
 */
@Repository
public interface AccountRepository extends MongoRepository<Account, Long> {
    /**
     * 查询账号
     * @param username 用户名
     * @return 账号
     */
    Account getByUsername(String username);
}
