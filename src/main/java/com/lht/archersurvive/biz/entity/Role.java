package com.lht.archersurvive.biz.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Role {
    private int serverId;
    private long roleId;
    private long lastLoginTime;
}
