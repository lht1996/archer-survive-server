package com.lht.archersurvive.biz.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * 账号信息
 *
 * @author 乐浩天
 */
@Document
@Data
@Builder
public class Account {
    @Id
    private long accountId;
    @Indexed
    private String username;
    private String password;
    private List<Role> roleList;
}
