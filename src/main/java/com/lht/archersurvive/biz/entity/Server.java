package com.lht.archersurvive.biz.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Server {
    private long id;
    private String type;
    private long heartbeat;
    private String name;
    private String host;
    private String port;
}
