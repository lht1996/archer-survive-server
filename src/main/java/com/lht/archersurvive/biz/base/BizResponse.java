package com.lht.archersurvive.biz.base;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BizResponse {
    private ResponseCode code;
    private String info;
    private Object data;

    public static BizResponse success() {
        return BizResponse.builder().code(ResponseCode.SUCCESS).info("成功！").build();
    }

    public static BizResponse success(Object data) {
        return BizResponse.builder().code(ResponseCode.SUCCESS).info("成功！").data(data).build();
    }

    public static BizResponse success(String info) {
        return BizResponse.builder().code(ResponseCode.SUCCESS).info(info).build();
    }

    public static BizResponse fail(String info) {
        return BizResponse.builder().code(ResponseCode.FAIL).info(info).build();
    }

    public static BizResponse fail(ResponseCode code, String info) {
        return BizResponse.builder().code(code).info(info).build();
    }
}
