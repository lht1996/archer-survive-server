package com.lht.archersurvive.biz.base;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public BizResponse handle(Exception e) {
        if (e instanceof BizException bizException) {
            return BizResponse.fail(bizException.getCode(), bizException.getInfo());
        } else {
            log.error("未知异常", e);
            return BizResponse.fail(ResponseCode.FAIL, "服务器内部错误，请联系管理员");
        }
    }
}
