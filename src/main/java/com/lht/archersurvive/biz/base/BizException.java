package com.lht.archersurvive.biz.base;

import lombok.Getter;

@Getter
public class BizException extends RuntimeException {
    private ResponseCode code;
    private String info;

    public BizException(ResponseCode code, String info) {
        this.code = code;
        this.info = info;
    }
}
