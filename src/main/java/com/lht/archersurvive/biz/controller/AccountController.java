package com.lht.archersurvive.biz.controller;

import com.lht.archersurvive.biz.base.BizException;
import com.lht.archersurvive.biz.base.BizResponse;
import com.lht.archersurvive.biz.base.ResponseCode;
import com.lht.archersurvive.biz.entity.Account;
import com.lht.archersurvive.biz.entity.Server;
import com.lht.archersurvive.biz.entity.ServerType;
import com.lht.archersurvive.biz.redis.RedisKey;
import com.lht.archersurvive.biz.repository.AccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Map;

/**
 * 账号服务
 *
 * @author 乐浩天
 */
@RestController
@Slf4j
public class AccountController {
    private final AccountRepository accountRepository;
    private final RedisTemplate<String, Object> redisTemplate;

    public AccountController(AccountRepository accountRepository, RedisTemplate<String, Object> redisTemplate) {
        this.accountRepository = accountRepository;
        this.redisTemplate = redisTemplate;
    }

    @PostMapping(value = "login", produces = MediaType.APPLICATION_JSON_VALUE)
    public BizResponse login(@RequestBody Account accountReq) {
        String username = accountReq.getUsername();
        String password = accountReq.getPassword();
        Account account = accountRepository.getByUsername(username);
        if (account == null) {
            Long accountId = redisTemplate.opsForValue().increment(RedisKey.ACCOUNT_INCREMENT.buildKey());
            if (accountId == null) {
                throw new BizException(ResponseCode.FAIL, "id生成失败");
            }
            Account newAccount = Account.builder()
                    .accountId(accountId)
                    .username(username)
                    .password(password)
                    .roleList(new ArrayList<>())
                    .build();
            accountRepository.save(newAccount);
        } else if (!account.getPassword().equals(password)) {
            throw new BizException(ResponseCode.FAIL, "密码错误");
        }
        Map<String, Server> entries = getServerMap();
//        Javers javers = JaversBuilder.javers()
//                .withListCompareAlgorithm(LEVENSHTEIN_DISTANCE)
//                .build();
//        javers.compare()
        return BizResponse.success(entries);
    }

    private Map<String, Server> getServerMap() {
        String key = RedisKey.SERVER_LIST.buildKey(ServerType.GAME.name());
        HashOperations<String, String, Server> operations = redisTemplate.opsForHash();
//        operations.put(key, "1", Server.builder().id(1).type(ServerType.GAME.name()).build());
        return operations.entries(key);
    }
}
