package com.lht.archersurvive.biz.redis;

import com.google.common.base.Joiner;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * redis key
 *
 * @author 乐浩天
 */
public enum RedisKey {
    /**
     * 账号ID自增
     */
    ACCOUNT_INCREMENT,
    /**
     * 服务列表
     */
    SERVER_LIST,
    ;

    public String buildKey(String... params) {
        LinkedList<String> strings = new LinkedList<>(Arrays.asList(params));
        strings.add(0, this.name());
        return Joiner.on(':').join(strings);
    }
}
